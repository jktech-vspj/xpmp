package com.example.listapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class editItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_edit_item);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        // Receive the parameters in the receiving activity
        Intent intent = getIntent();
        String itemName = intent.getStringExtra("itemName");
        String itemAmount = intent.getStringExtra("itemAmount");

        EditText nameEditText = findViewById(R.id.editTextName);
        EditText ammountEditText = findViewById(R.id.editTextWeight);

        nameEditText.setText(itemName);
        ammountEditText.setText(itemAmount);

        Button deleteButton = findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper db = new DatabaseHelper(editItemActivity.this);
                db.deleteItem(itemName);

                Intent myIntent = new Intent(editItemActivity.this, MainActivity.class);
                startActivity(myIntent);

                Toast.makeText(editItemActivity.this, "Položka byla odebrána", Toast.LENGTH_SHORT).show();
            }
        });

        Button editButton = findViewById(R.id.buttonSubmit);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper db = new DatabaseHelper(editItemActivity.this);
                db.updateItem(itemName, nameEditText.getText().toString(), ammountEditText.getText().toString());

                Intent myIntent = new Intent(editItemActivity.this, MainActivity.class);
                startActivity(myIntent);

                Toast.makeText(editItemActivity.this, "Položka byla upravena", Toast.LENGTH_SHORT).show();
            }
        });
    }
}