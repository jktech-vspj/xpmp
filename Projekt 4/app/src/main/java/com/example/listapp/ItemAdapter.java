package com.example.listapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    private List<Item> itemList;
    private Context context;

    public ItemAdapter(Context context, List<Item> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Item item = itemList.get(position);
        holder.itemNameTextView.setText(item.getName());
        holder.itemAmountTextView.setText(item.getAmount());
        holder.checkBox.setChecked(item.getStatus());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemNameTextView;
        TextView itemAmountTextView;
        CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemNameTextView = itemView.findViewById(R.id.itemNameTextView);
            itemAmountTextView = itemView.findViewById(R.id.itemAmountTextView);
            checkBox = itemView.findViewById(R.id.checkBox);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = itemView.getContext();
                    Intent myIntent = new Intent(context, editItemActivity.class);
                    myIntent.putExtra("itemName", itemNameTextView.getText().toString());
                    myIntent.putExtra("itemAmount", itemAmountTextView.getText().toString());
                    context.startActivity(myIntent);
                }
            });

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Context context = itemView.getContext();
                    DatabaseHelper db = new DatabaseHelper(context);
                    if(isChecked) {
                        itemNameTextView.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        itemAmountTextView.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        db.setItemStatus(itemNameTextView.getText().toString(), true);
                    }

                    else {
                        itemNameTextView.setPaintFlags(Paint.CURSOR_AFTER);
                        itemAmountTextView.setPaintFlags(Paint.CURSOR_AFTER);
                        db.setItemStatus(itemNameTextView.getText().toString(), false);
                    }
                }
            });
        }
    }
}
