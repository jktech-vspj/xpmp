package com.example.listapp;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import android.content.ContentValues;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "list_app.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "shoping_list";
    private static final String COLUMN_SL_ID = "_id";
    private static final String COLUMN_SL_NAME = "name";
    private static final String COLUMN_SL_AMOUNT = "amount";
    private static final String COLUMN_SL_STATUS = "status";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_SL_ID + " INTEGER PRIMARY KEY," +
                COLUMN_SL_NAME + " TEXT," +
                COLUMN_SL_AMOUNT + " TEXT," +
                COLUMN_SL_STATUS + " INTEGER DEFAULT 0)";
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public ArrayList<Item> getItems() {
        ArrayList<Item> itemList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = { COLUMN_SL_ID, COLUMN_SL_NAME, COLUMN_SL_AMOUNT, COLUMN_SL_STATUS };
        Cursor cursor = db.query(TABLE_NAME, projection, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                int idIndex = cursor.getColumnIndex(COLUMN_SL_ID);
                int nameIndex = cursor.getColumnIndex(COLUMN_SL_NAME);
                int amountIndex = cursor.getColumnIndex(COLUMN_SL_AMOUNT);
                int statusIndex = cursor.getColumnIndex(COLUMN_SL_STATUS);
                if(idIndex >= 0) {
                    int id = cursor.getInt(idIndex);
                    String name = cursor.getString(nameIndex);
                    String amount = cursor.getString(amountIndex);
                    boolean status = cursor.getInt(statusIndex) == 0;

                    itemList.add(new Item(name, amount, status, id));
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return itemList;
    }

    public boolean addItem(String itemName, String itemAmount, boolean itemStatus) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SL_NAME, itemName);
        values.put(COLUMN_SL_AMOUNT, itemAmount);
        values.put(COLUMN_SL_STATUS, itemStatus);

        long newRowId = db.insert(TABLE_NAME, null, values);

        return newRowId != -1;
    }

    public boolean setItemStatus(String itemName, boolean newStatus) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SL_STATUS, newStatus);

        String selection = COLUMN_SL_NAME + " = ?";
        String[] selectionArgs = { String.valueOf(itemName) };

        int rowsAffected = db.update(TABLE_NAME, values, selection, selectionArgs);

        return rowsAffected > 0;
    }

    public boolean deleteItem(String itemName) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = COLUMN_SL_NAME + " = ?";
        String[] selectionArgs = { String.valueOf(itemName) };

        int rowsAffected = db.delete(TABLE_NAME, selection, selectionArgs);

        return rowsAffected > 0;
    }

    public boolean updateItem(String oldName, String newName, String newAmount) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SL_NAME, newName);
        values.put(COLUMN_SL_AMOUNT, newAmount);

        String selection = COLUMN_SL_NAME + " = ?";
        String[] selectionArgs = { String.valueOf(oldName) };

        int rowsAffected = db.update(TABLE_NAME, values, selection, selectionArgs);

        return rowsAffected > 0;
    }
}
