package com.example.listapp;

public class Item {
    private String name;
    private String amount;
    private boolean selected;
    private int id;

    public Item(String name, String amount, boolean selected) {
        this.name = name;
        this.amount = amount;
        this.selected = selected;
    }

    public Item(String name, String amount, boolean selected, int id) {
        this.name = name;
        this.amount = amount;
        this.selected = selected;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getAmount() {
        return amount;
    }

    public boolean getStatus() {
        return !selected;
    }

    public int getId() {
        return  id;
    }
}

