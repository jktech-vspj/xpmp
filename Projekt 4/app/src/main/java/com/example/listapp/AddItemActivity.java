package com.example.listapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class AddItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_add_item);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Button submitButton = findViewById(R.id.buttonSubmit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText itemNameElement = findViewById(R.id.editTextName);
                EditText itemWeightElement = findViewById(R.id.editTextWeight);

                String itemName = itemNameElement.getText().toString();
                String itemAmount = itemWeightElement.getText().toString();

                if(itemName.trim().isEmpty()) {
                    Toast.makeText(AddItemActivity.this, "Zadejte platný název", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(itemAmount.trim().isEmpty()) {
                    Toast.makeText(AddItemActivity.this, "Zadejte platné množství", Toast.LENGTH_SHORT).show();
                    return;
                }

                DatabaseHelper db = new DatabaseHelper(AddItemActivity.this);
                db.addItem(itemName, itemAmount, false);

                Intent myIntent = new Intent(AddItemActivity.this, MainActivity.class);
                startActivity(myIntent);

                Toast.makeText(AddItemActivity.this, "Položka byla přidána", Toast.LENGTH_SHORT).show();
            }
        });
    }
}