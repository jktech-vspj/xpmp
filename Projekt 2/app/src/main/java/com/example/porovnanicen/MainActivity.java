package com.example.porovnanicen;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.*;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Button button = findViewById(R.id.buttonCompare);
        button.setOnClickListener(v -> {
            compare();
        });
    }

    private void compare() {
        // Product 1
        EditText p1PriceView = findViewById(R.id.editTextProduct1Price);
        EditText p1WeightView = findViewById(R.id.editTextProduct1weight);

        double product1Price, product1Weight;

        try {
            product1Price = Double.parseDouble(p1PriceView.getText().toString());
        }

        catch (NumberFormatException e) {
            displayDialog("Neplatná cena", "Neplatná hodnota hodnota ceny pro produkt 1");
            p1PriceView.requestFocus();
            return;
        }

        if(product1Price <= 0) {
            displayDialog("Neplatná cena", "Neplatná hodnota hodnota ceny pro produkt 1");
            p1PriceView.requestFocus();
            return;
        }

        try {
            product1Weight = Double.parseDouble(p1WeightView.getText().toString());
        }

        catch (NumberFormatException e) {
            displayDialog("Neplatná gramáž", "Neplatná gramáž pro produkt 1");
            p1WeightView.requestFocus();
            return;
        }

        if(product1Weight <= 0) {
            displayDialog("Neplatná gramáž", "Neplatná gramáž pro produkt 1");
            p1WeightView.requestFocus();
            return;
        }

        // Product 2
        EditText p2PriceView = findViewById(R.id.editTextProduct2Price);
        EditText p2WeightView = findViewById(R.id.editTextProduct2weight);

        double product2Price, product2Weight;

        try {
            product2Price = Double.parseDouble(p2PriceView.getText().toString());
        }

        catch (NumberFormatException e) {
            displayDialog("Neplatná cena", "Neplatná hodnota hodnota ceny pro produkt 2");
            p2PriceView.requestFocus();
            return;
        }

        if(product2Price <= 0) {
            displayDialog("Neplatná cena", "Neplatná hodnota hodnota ceny pro produkt 2");
            p2PriceView.requestFocus();
            return;
        }

        try {
            product2Weight = Double.parseDouble(p2WeightView.getText().toString());
        }

        catch (NumberFormatException e) {
            displayDialog("Neplatná gramáž", "Neplatná gramáž pro produkt 2");
            p2WeightView.requestFocus();
            return;
        }

        if(product2Weight <= 0) {
            displayDialog("Neplatná gramáž", "Neplatná gramáž pro produkt 2");
            p2WeightView.requestFocus();
            return;
        }

        TextView resultView = findViewById(R.id.textViewResult);
        clearResults();

        if((product1Price * product1Weight) == (product2Price * product2Weight)) {
            //TODO Ceny jsou rovny
            resultView.setText("Cena produktů za jednotku je stejná");
        }

        else if((product1Price * product1Weight) > (product2Price * product2Weight)) {
            //TODO Cena za produkt 1 je vyšší
            resultView.setText("Cena produktů za jednotku je levnější pro produkt 2");
            LinearLayout product2Box = findViewById(R.id.product2Layout);
            product2Box.setBackgroundColor(Color.rgb(120, 255, 120));
        }

        else if((product1Price * product1Weight) < (product2Price * product2Weight)) {
            //TODO Cena za produkt 2 je vyšší
            resultView.setText("Cena produktů za jednotku je levnější pro produkt 1");
            LinearLayout product1Box = findViewById(R.id.product1Layout);
            product1Box.setBackgroundColor(Color.rgb(120, 255, 120));
        }
    }

    private void clearResults() {
        TextView resultView = findViewById(R.id.textViewResult);
        resultView.setText("");

        LinearLayout product1Box = findViewById(R.id.product1Layout);
        product1Box.setBackgroundColor(Color.TRANSPARENT);

        LinearLayout product2Box = findViewById(R.id.product2Layout);
        product2Box.setBackgroundColor(Color.TRANSPARENT);
    }

    private void displayDialog(String title, String text) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(title);
        dialog.setMessage(text);
        dialog.setPositiveButton("OK", null);
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }
}