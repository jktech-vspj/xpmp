package com.example.bank_app;

public class User {
    private final long id;
    private final int bank_code;
    private final String email;
    private final String firstname;
    private final String lastname;
    private double balance;

    public User(long id, int bank_code, String email, String firstname, String lastname, double balance) {
        this.id = id;
        this.bank_code = bank_code;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }
    public String getEmail() {
        return email;
    }
    public double getBalance() {
        return balance;
    }
    public String getLastname() {
        return lastname;
    }

    public int getBank_code() {
        return bank_code;
    }
    public String getFirstname() {
        return firstname;
    }
}

