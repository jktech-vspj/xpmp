package com.example.bank_app;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;


public class qrActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_qr);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Button generateQrButton = findViewById(R.id.generate_qr);
        generateQrButton.setOnClickListener(v -> {
            String amount = findViewById(R.id.id_qr_amount).toString();

            if(amount.isEmpty()) {
                Toast.makeText(qrActivity.this, R.string.wrong_amount, Toast.LENGTH_SHORT).show();
                return;
            }

            String message = findViewById(R.id.id_qr_message).toString();
            generateQr(message, amount);
        });
    }

    protected void generateQr(String message, String amount) {
        ImageView qrImageView = findViewById(R.id.qr_image_view);

        String textToEncode = "SPD*1.0*ACC:CZ2806000000000000000123*AM:" + amount + "*CC:CZK*MSG:" + message + "*X-VS:1234567890!";

        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            BitMatrix bitMatrix = barcodeEncoder.encode(textToEncode, BarcodeFormat.QR_CODE, 400, 400);
            Bitmap bitmap = Bitmap.createBitmap(400, 400, Bitmap.Config.RGB_565);

            for (int x = 0; x < 400; x++) {
                for (int y = 0; y < 400; y++) {
                    bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }

            qrImageView.setImageBitmap(bitmap);

        }

        catch (WriterException e) {
            e.printStackTrace();
        }
    }
}