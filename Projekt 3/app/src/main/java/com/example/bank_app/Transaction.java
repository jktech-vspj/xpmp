package com.example.bank_app;

public class Transaction {
    private String source;
    private String messageSource;
    private long amount;
    private String date;

    public Transaction(String source, String messageSource, long amount, String date) {
        this.source = source;
        this.messageSource = messageSource;
        this.amount = amount;
        this.date = date;
    }

    @Override
    public String toString() {
        return String.format("Detaily transakce:\n" +
                "Zdroj: %s\n" +
                "Zpráva: %s\n" +
                "Částka: %d\n" +
                "Datum: %s\n" +
                "----------------------------------\n\n", source, messageSource, amount, date);
    }
}

