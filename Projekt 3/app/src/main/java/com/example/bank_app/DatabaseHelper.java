package com.example.bank_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.util.Base64;

import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "bank_app.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_USERS = "users";
    private static final String COLUMN_ACCOUNT_ID = "_id";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_BALANCE = "balance";
    private static final String COLUMN_BANK_CODE = "bank_code";
    private static final String COLUMN_PASSWORD = "password";

    private static final String COLUMN_FIRSTNAME = "firstname";
    private static final String COLUMN_LASTNAME = "lastname";

    /*Table transactions*/
    private static final String TABLE_TRANSACTIONS = "transactions";
    private static final String COLUMN_TRANSACTION_ID = "_id";
    private static final String COLUMN_TRANSACTION_TARGET_ACCOUNT = "target_account";
    private static final String COLUMN_TRANSACTION_TARGET_BANK = "target_bank";
    private static final String COLUMN_TRANSACTION_TARGET_MESSAGE = "target_message";
    private static final String COLUMN_TRANSACTION_SOURCE = "source";
    private static final String COLUMN_TRANSACTION_SOURCE_MESSAGE = "source_message";
    private static final String COLUMN_TRANSACTION_AMOUNT = "amount";
    private static final String COLUMN_TRANSACTION_DATE = "date";

    private static final String CREATE_USERS_TABLE =
            "CREATE TABLE " + TABLE_USERS + "("
                    + COLUMN_ACCOUNT_ID + " INTEGER PRIMARY KEY,"
                    + COLUMN_BANK_CODE + " INTEGER,"
                    + COLUMN_EMAIL + " TEXT,"
                    + COLUMN_FIRSTNAME + " TEXT,"
                    + COLUMN_LASTNAME + " TEXT,"
                    + COLUMN_PASSWORD + " TEXT,"
                    + COLUMN_BALANCE + " REAL);";

    private static final String CREATE_TRANSACTIONS_TABLE =
            "CREATE TABLE " + TABLE_TRANSACTIONS + "("
                    + COLUMN_TRANSACTION_ID + " INTEGER PRIMARY KEY,"
                    + COLUMN_TRANSACTION_TARGET_MESSAGE + " TEXT, "
                    + COLUMN_TRANSACTION_SOURCE_MESSAGE + " TEXT, "
                    + COLUMN_TRANSACTION_TARGET_ACCOUNT + " INTEGER,"
                    + COLUMN_TRANSACTION_TARGET_BANK + " INTEGER,"
                    + COLUMN_TRANSACTION_SOURCE + " TEXT,"
                    + COLUMN_TRANSACTION_DATE + " TEXT,"
                    + COLUMN_TRANSACTION_AMOUNT + " TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_TRANSACTIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_TRANSACTIONS_TABLE);
    }

    public void addUser(SQLiteDatabase db, long id, int bankCode, String email, String password, String firstname, String lastname, double balance) {
        String passwordHash = hashPassword(password);

        if(bankCode < 1000 || bankCode > 9999) {
            throw new InvalidParameterException("Invalid bank code");
        }

        ContentValues values = new ContentValues();
        values.put(COLUMN_ACCOUNT_ID, id);
        values.put(COLUMN_BANK_CODE, bankCode);
        values.put(COLUMN_EMAIL, email);
        values.put(COLUMN_FIRSTNAME, firstname);
        values.put(COLUMN_LASTNAME, lastname);
        values.put(COLUMN_PASSWORD, passwordHash);
        values.put(COLUMN_BALANCE, balance);

        long status = db.insert(TABLE_USERS, null, values);
        if (status == -1) {
            throw new InvalidParameterException("Unable to create user");
        }
    }

    public long checkUserCredentials(String email, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        String hashedPassword = hashPassword(password);
        Cursor cursor;

         cursor = db.query(
                TABLE_USERS,
                new String[]{COLUMN_ACCOUNT_ID},
                COLUMN_EMAIL + "=? AND " + COLUMN_PASSWORD + "=?",
                new String[]{email, hashedPassword},
                null, null, null, "1");

        long userId = -1;

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            int idIndex = cursor.getColumnIndex(COLUMN_ACCOUNT_ID);
            if (idIndex != -1) {
                userId = cursor.getLong(idIndex);
            }
        }

        cursor.close();
        db.close();

        return userId;
    }
    public User getAccountData(long accountNumber) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                true,
                TABLE_USERS,
                new String[]{COLUMN_ACCOUNT_ID, COLUMN_EMAIL, COLUMN_FIRSTNAME, COLUMN_LASTNAME, COLUMN_BALANCE, COLUMN_BANK_CODE},
                COLUMN_ACCOUNT_ID + "=?",
                new String[]{Long.toString(accountNumber)},
                null, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            int id_index = cursor.getColumnIndex(COLUMN_ACCOUNT_ID);
            int bank_code_index = cursor.getColumnIndex(COLUMN_BANK_CODE);
            int email_index = cursor.getColumnIndex(COLUMN_EMAIL);
            int firstname_index = cursor.getColumnIndex(COLUMN_FIRSTNAME);
            int lastname_index = cursor.getColumnIndex(COLUMN_LASTNAME);
            int balance_index = cursor.getColumnIndex(COLUMN_BALANCE);

            if(id_index != -1 && email_index != -1 && firstname_index != -1 && lastname_index != -1 && balance_index != -1) {
                long id = cursor.getLong(id_index);
                int bank_code = cursor.getInt(bank_code_index);
                String email = cursor.getString(email_index);
                String firstname = cursor.getString(firstname_index);
                String lastname = cursor.getString(lastname_index);
                double balance = cursor.getDouble(balance_index);
                cursor.close();

                return new User(id, bank_code, email, firstname, lastname, balance);
            }

            db.close();
            throw new InvalidParameterException("Unable to get account data");
        }

        db.close();

        throw new InvalidParameterException("Account not found");
    }

    public String hashPassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            return Base64.encodeToString(hash, Base64.NO_WRAP);
        }

        catch (Exception e) {
            throw new RuntimeException("Hash algorithm not found", e);
        }
    }
    public double getBalance(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        double balance = 0;

        Cursor cursor = db.query(
                TABLE_USERS,
                new String[] { COLUMN_BALANCE },
                COLUMN_ACCOUNT_ID + " = ?",
                new String[] { Long.toString(id) },
                null, null, null);

        if (cursor.moveToFirst()) {
            int balanceColumnIndex = cursor.getColumnIndex(COLUMN_BALANCE);
            if (balanceColumnIndex != -1) {
                balance = cursor.getDouble(balanceColumnIndex);
            }

            else {

            }
        }
        cursor.close();
        db.close();
        return balance;
    }

    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USERS, null);
        if (cursor != null) {
            int idIndex = cursor.getColumnIndex(COLUMN_ACCOUNT_ID);
            int bankCodeIndex = cursor.getColumnIndex(COLUMN_BANK_CODE);
            int emailIndex = cursor.getColumnIndex(COLUMN_EMAIL);
            int firstnameIndex = cursor.getColumnIndex(COLUMN_FIRSTNAME);
            int lastnameIndex = cursor.getColumnIndex(COLUMN_LASTNAME);
            int balanceIndex = cursor.getColumnIndex(COLUMN_BALANCE);

            if (idIndex != -1 && bankCodeIndex != -1 && emailIndex != -1 && firstnameIndex != -1 && lastnameIndex != -1 && balanceIndex != -1) {
                while (cursor.moveToNext()) {
                    long id = cursor.getLong(idIndex);
                    int bankCode = cursor.getInt(bankCodeIndex);
                    String email = cursor.getString(emailIndex);
                    String firstname = cursor.getString(firstnameIndex);
                    String lastname = cursor.getString(lastnameIndex);
                    double balance = cursor.getDouble(balanceIndex);

                    User user = new User(id, bankCode, email, firstname, lastname, balance);
                    userList.add(user);
                }
            }

            cursor.close();
        }

        db.close();
        return userList;
    }
    public void addPayment(long sourceAccount, long targetAccount, long amount, String messageSource, String messageTarget, String date) {
        double sourceBalance = getBalance(sourceAccount);

        if (sourceBalance < amount) {
            throw new InvalidParameterException("Insufficient funds");
        }

        double newSourceBalance = sourceBalance - amount;
        updateBalance(Long.toString(sourceAccount), newSourceBalance);

        double targetBalance = getBalance(targetAccount);
        double newTargetBalance = targetBalance + amount;
        updateBalance(Long.toString(targetAccount), newTargetBalance);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues transactionValues = new ContentValues();
        transactionValues.put(COLUMN_TRANSACTION_TARGET_ACCOUNT, targetAccount);
        transactionValues.put(COLUMN_TRANSACTION_TARGET_BANK, 7777);
        transactionValues.put(COLUMN_TRANSACTION_TARGET_MESSAGE, messageTarget);
        transactionValues.put(COLUMN_TRANSACTION_SOURCE, sourceAccount);
        transactionValues.put(COLUMN_TRANSACTION_SOURCE_MESSAGE, messageSource);
        transactionValues.put(COLUMN_TRANSACTION_DATE, date);
        transactionValues.put(COLUMN_TRANSACTION_AMOUNT, amount);
        long transactionStatus = db.insert(TABLE_TRANSACTIONS, null, transactionValues);
        if (transactionStatus == -1) {
            db.close();
            throw new InvalidParameterException("Unable to add transaction record");
        }

        db.close();
    }

    public List<Transaction> getAllTransactionsForAccount(long accountId) {
        List<Transaction> transactions = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
            TABLE_TRANSACTIONS,
            null,
            COLUMN_TRANSACTION_SOURCE + "=? OR " + COLUMN_TRANSACTION_TARGET_ACCOUNT + "=?",
            new String[]{Long.toString(accountId), Long.toString(accountId)},
            null,
            null,
            COLUMN_TRANSACTION_DATE + " DESC");

        int sourceAccountIndex = cursor.getColumnIndex(COLUMN_TRANSACTION_SOURCE);
        int targetAccountIndex = cursor.getColumnIndex(COLUMN_TRANSACTION_TARGET_ACCOUNT);
        int messageSourceIndex = cursor.getColumnIndex(COLUMN_TRANSACTION_SOURCE);
        int amountSourceIndex = cursor.getColumnIndex(COLUMN_TRANSACTION_AMOUNT);
        int dateIndex = cursor.getColumnIndex(COLUMN_TRANSACTION_DATE);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                long sourceAccount = cursor.getLong(sourceAccountIndex);
                long targetAccount = cursor.getLong(targetAccountIndex);
                String source = (sourceAccount == accountId) ? "From" : "To";
                String messageSource = cursor.getString(messageSourceIndex);
                long amount = cursor.getLong(amountSourceIndex);
                String date = cursor.getString(dateIndex);

                Transaction transaction = new Transaction(source, messageSource, amount, date);
                transactions.add(transaction);
            } while (cursor.moveToNext());

            cursor.close();
        }

        db.close();
        return transactions;
    }
    public void updateBalance(String id, double newBalance) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_BALANCE, newBalance);

        db.update(
                TABLE_USERS,
                values,
                COLUMN_ACCOUNT_ID + " = ?",
                new String[] { id });

        db.close();
    }
}

