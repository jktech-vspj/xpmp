package com.example.bank_app;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class activity_submit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_submit);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        long sourceAccount = getIntent().getLongExtra("source_account", 0);
        long targetAccount = getIntent().getLongExtra("target_account", 0);
        String targetBank = getIntent().getStringExtra("target_bank");
        long targetAmount = getIntent().getLongExtra("target_amount", 0);
        String targetMessage = getIntent().getStringExtra("target_message");
        String sourceMessage = getIntent().getStringExtra("source_message");
        String date = getIntent().getStringExtra("date");

        EditText targetAccountView = findViewById(R.id.id_accept_account_number);
        EditText targetBankView = findViewById(R.id.id_accept_bank);
        EditText targetAmountView = findViewById(R.id.id_accept_amount);
        EditText targetMessageView = findViewById(R.id.id_accept_target_message);
        EditText sourceMessageView = findViewById(R.id.id_accept_source_message);
        EditText dateView = findViewById(R.id.id_accept_date);

        targetAccountView.setText(Long.toString(targetAccount));
        targetBankView.setText(targetBank);
        targetAmountView.setText(Long.toString(targetAmount));
        targetMessageView.setText(targetMessage);
        sourceMessageView.setText(sourceMessage);
        dateView.setText(date);

        Button accept = findViewById(R.id.id_accept_submit);
        accept.setOnClickListener(v -> {
            final DatabaseHelper dbHelper = new DatabaseHelper(this);
            dbHelper.addPayment(sourceAccount, targetAccount, targetAmount, sourceMessage, targetMessage, date);

            User usr = dbHelper.getAccountData(sourceAccount);

            Intent intent = new Intent(activity_submit.this, WelcomeActivity.class);
            intent.putExtra("account_id", usr.getId());
            intent.putExtra("account_bank_code", usr.getBank_code());
            intent.putExtra("account_firstname", usr.getFirstname());
            intent.putExtra("account_lastname", usr.getLastname());
            intent.putExtra("account_balance", usr.getBalance());
            startActivity(intent);
        });
    }
}