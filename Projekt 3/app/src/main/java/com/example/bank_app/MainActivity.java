package com.example.bank_app;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DatabaseHelper dbHelper = new DatabaseHelper(this);

        // Run database operations in a separate thread to prevent UI blocking
        new Thread(new Runnable() {
            @Override
            public void run() {
                initializeDefaultUsers(dbHelper);
            }
        }).start();

        final EditText emailEditText = findViewById(R.id.id_email);
        final EditText passwordEditText = findViewById(R.id.id_password);
        Button loginButton = findViewById(R.id.login_click);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailEditText.getText().toString().trim();

                if(email.isEmpty()) {
                    Toast.makeText(MainActivity.this, R.string.empty_input, Toast.LENGTH_SHORT).show();
                    emailEditText.requestFocus();
                    return;
                }

                String password = passwordEditText.getText().toString().trim();

                if(password.isEmpty()) {
                    Toast.makeText(MainActivity.this, R.string.empty_input, Toast.LENGTH_SHORT).show();
                    passwordEditText.requestFocus();
                    return;
                }

                final long userVerify = dbHelper.checkUserCredentials(email, password);
                if (userVerify != -1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            User usr = dbHelper.getAccountData(userVerify);

                            Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
                            intent.putExtra("account_id", usr.getId());
                            intent.putExtra("account_bank_code", usr.getBank_code());
                            intent.putExtra("account_firstname", usr.getFirstname());
                            intent.putExtra("account_lastname", usr.getLastname());
                            intent.putExtra("account_balance", usr.getBalance());
                            startActivity(intent);
                        }
                    });
                }

                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, R.string.invalid_credentials, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void initializeDefaultUsers(DatabaseHelper dbHelper) {
        SharedPreferences prefs = getSharedPreferences("AppPrefs", MODE_PRIVATE);
        if (!prefs.getBoolean("isInitialized", false)) {
            // Open the database, begin the transaction, and close in finally block
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            try {
                // Hash the passwords before storing them
                dbHelper.addUser(db, 854793256, 7777, "test@test.cz", "test",  "Pepa", "Depa", 200);
                dbHelper.addUser(db, 687413688, 6587, "another@user.cz", "test",  "Jiný", "Pepa", 450);
                db.setTransactionSuccessful();
            }

            catch (Exception e) {
                Log.e("ERROR", "Unable to create default users", e);
            }

            finally {
                db.endTransaction();
                db.close();

                // Mark initialization as done
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("isInitialized", true);
                editor.apply();
            }
        }
    }
}