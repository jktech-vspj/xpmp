package com.example.bank_app;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PayActivity extends AppCompatActivity {
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        dbHelper = new DatabaseHelper(this);
        long account_number = getIntent().getLongExtra("account_number", 0);

        List<User> userList = dbHelper.getAllUsers();
        Spinner sp = findViewById(R.id.id_pay_account_number);

        List<String> accountNumbers = new ArrayList<>();

        User usr = dbHelper.getAccountData(account_number);

        for (User user : userList) {
            if(usr.getId() != user.getId()) {
                accountNumbers.add(Long.toString(user.getId()));
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, accountNumbers);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp.setAdapter(adapter);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String date = sdf.format(new Date());

        EditText targetDate = findViewById(R.id.id_pay_date);
        targetDate.setText(date);

        Button payButton = findViewById(R.id.id_pay_submit);
        payButton.setOnClickListener(v ->sendMoney(account_number));

        Button copyButton = findViewById(R.id.id_pay_copy_message);
        copyButton.setOnClickListener(v ->copyMessage());
    }

    protected void sendMoney(long accountNumber) {
        User usr = dbHelper.getAccountData(accountNumber);

        Spinner targetSpinner = findViewById(R.id.id_pay_account_number);
        long targetAccount = Long.parseLong((String) targetSpinner.getSelectedItem());

        Spinner targetBankCodeView = findViewById(R.id.id_pay_banks);
        String targetBank = (String) targetBankCodeView.getSelectedItem();

        EditText targetAmountVIew = findViewById(R.id.id_pay_amount);
        String targetAmountString = targetAmountVIew.getText().toString().trim();

        if(targetAmountString.isEmpty()) {
            Toast.makeText(PayActivity.this, R.string.empty_input, Toast.LENGTH_SHORT).show();
            return;
        }

        long targetAmount = Long.parseLong(targetAmountString);
        if(targetAmount > usr.getBalance()) {
            Toast.makeText(PayActivity.this, R.string.amount_out, Toast.LENGTH_SHORT).show();
            return;
        }

        EditText targetMessageView = findViewById(R.id.id_pay_target_message);
        EditText sourceMessageView = findViewById(R.id.id_pay_source_message);
        EditText dateView = findViewById(R.id.id_pay_date);

        Intent intent = new Intent(PayActivity.this, activity_submit.class);
        intent.putExtra("source_account", accountNumber);
        intent.putExtra("target_account", targetAccount);
        intent.putExtra("target_bank", targetBank);
        intent.putExtra("target_amount", targetAmount);
        intent.putExtra("target_message", targetMessageView.getText().toString());
        intent.putExtra("source_message", sourceMessageView.getText().toString());
        intent.putExtra("date", dateView.getText().toString());

        startActivity(intent);
    }

    private void copyMessage() {
        EditText targetMessageView = findViewById(R.id.id_pay_target_message);
        String targetMessage = targetMessageView.getText().toString();
        EditText targetMesageView = findViewById(R.id.id_pay_source_message);
        targetMesageView.setText(targetMessage);
    }
}
