package com.example.bank_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);

        long account_number = getIntent().getLongExtra("account_id", 0);
        final DatabaseHelper dbHelper = new DatabaseHelper(this);
        User usr = dbHelper.getAccountData(account_number);

        TextView welcomeText = findViewById(R.id.welcome_text);
        welcomeText.setText(getString(R.string.welcome) + ", " + usr.getFirstname() + " " + usr.getLastname());

        TextView anView = findViewById(R.id.account_number_text);
        anView.setText(Long.toString(account_number) + "/" + usr.getBank_code());

        TextView balanceView = findViewById(R.id.balance_text);
        balanceView.setText(Double.toString(usr.getBalance()) + "Kč");

        TextView transactionsView = findViewById(R.id.id_transactions);
        List<Transaction> ts = dbHelper.getAllTransactionsForAccount(account_number);
        transactionsView.setText(ts.toString());

        Button payButton = findViewById(R.id.id_pay);
        payButton.setOnClickListener(v -> payActivity(account_number));

        Button qrButton = findViewById(R.id.id_qr);
        qrButton.setOnClickListener(v -> qrActivity());

        Button logoutButton = findViewById(R.id.id_logout);
        logoutButton.setOnClickListener(v -> logoutActivity());
    }

    protected void payActivity(long accountNumber) {
        Intent intent = new Intent(WelcomeActivity.this, PayActivity.class);
        intent.putExtra("account_number", accountNumber);
        startActivity(intent);
    }

    protected void qrActivity() {
        Intent intent = new Intent(WelcomeActivity.this, qrActivity.class);
        startActivity(intent);
    }

    protected void logoutActivity() {
        Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}