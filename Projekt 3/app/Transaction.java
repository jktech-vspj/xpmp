public class Transaction {
    private String source;
    private String messageSource;
    private long amount;
    private String date;

    public Transaction(String source, String messageSource, long amount, String date) {
        this.source = source;
        this.messageSource = messageSource;
        this.amount = amount;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "source='" + source + '\'' +
                ", messageSource='" + messageSource + '\'' +
                ", amount=" + amount +
                ", date='" + date + '\'' +
                '}';
    }
}
