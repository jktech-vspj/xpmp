package com.example.bmi;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        Button button = findViewById(R.id.calculateButton);
        button.setOnClickListener(v -> calculateBMI());
    }

    private void calculateBMI() {
        TextView resultView = findViewById(R.id.resultBox);
        resultView.setText("");

        TextView weightView = findViewById(R.id.weightInput);
        TextView heightView = findViewById(R.id.heightInput);

        double weight, height;
        try {
            weight = Double.parseDouble(weightView.getText().toString());
        }

        catch (NumberFormatException e) {
            Toast.makeText(MainActivity.this, R.string.wrongWeight, Toast.LENGTH_SHORT).show();
            weightView.requestFocus();
            return;
        }

        if(weight <= 0) {
            Toast.makeText(MainActivity.this, R.string.wrongWeight, Toast.LENGTH_SHORT).show();
            weightView.requestFocus();
            return;
        }

        try {
            height = Double.parseDouble(heightView.getText().toString());
        }

        catch (NumberFormatException e) {
            Toast.makeText(MainActivity.this, R.string.wrongHeight, Toast.LENGTH_SHORT).show();
            heightView.requestFocus();
            return;
        }

        if(height <= 0) {
            Toast.makeText(MainActivity.this, R.string.wrongHeight, Toast.LENGTH_SHORT).show();
            heightView.requestFocus();
            return;
        }

        height /= 100;

        double result = weight / Math.pow(height, 2);
        @SuppressLint("DefaultLocale")
        String resultText = getString(R.string.resultPrefix) + String.format("%.1f", result);
        resultView.setText(resultText);
    }
}