package com.example.thread_app;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {
    private EditText inputEditText;
    private TextView resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        inputEditText = findViewById(R.id.inputEditText);
        resultTextView = findViewById(R.id.resultTextView);

        Button calculateButton = findViewById(R.id.calculateButton);
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateFactorial();
            }
        });
    }
    private void calculateFactorial() {
        String inputStr = inputEditText.getText().toString();
        try {
            int number = Integer.parseInt(inputStr);
            if (number < 0) {
                showErrorDialog("Zadejte nezáporné číslo.");
                return;
            }

            // Vytvoření nového vlákna pro výpočet faktoriálu
            new Thread(new Runnable() {
                @Override
                public void run() {
                    long factorial = calculateFactorial(number);
                    // Zobrazení výsledku v hlavním vlákně
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            resultTextView.setText("Faktoriál " + number + " = " + factorial);
                        }
                    });
                }
            }).start();
        } catch (NumberFormatException e) {
            showErrorDialog("Neplatný vstup.");
        }
    }

    private long calculateFactorial(int n) {
        long factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }

    private void showErrorDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Chyba");
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
}